# rscanner

Rscanner is a simple radial-like scanner using a lidar module and ST Discovery platform.
This repo contains all the code required for the project divided into "modules."
Each module contains licencing info.

## Project structure

- **firmware** -- this directory contains the firmware code for the platform.
- **client** -- this direcotry contains the client software source code.
- **docs** -- this directory contains all the documentation.